# Ansible Meetup - Molecule 3 demo

[![pipeline status](https://gitlab.com/rgerardi/molecule3-demo/badges/master/pipeline.svg)](https://gitlab.com/rgerardi/molecule3-demo/-/commits/master)

## Usefule links

[Molecule documentation](https://molecule.readthedocs.io/)

[Molecule v3 migration checklist](https://github.com/ansible-community/molecule/issues/2560)

[Molecule v3 CI Integration examples](https://molecule.readthedocs.io/en/latest/ci.html)


## Demo Workflow

1. Create virtualenv

```
python3 -m venv molecule-venv
```

2. Install molecule

```
pip install "molecule[lint]"
export PATH=$(pwd):$PATH
```

3. Init role with podman driver

```
molecule init role mywebapp --driver-name=podman
```

4. Add linter to config `molecule/default/molecule.yml`

```
lint: |
  set -e
  yamllint .
  ansible-lint .
```

5. Lint project

```
molecule lint
```

6. Fix the issues
7. Lint project again

```
molecule lint
```

8. Pre-download image (if required)
9. Configure molecule `molecule/default/molecule.yml` platform and provisioner

```
platforms:
  - name: centos
    image: centos:8
    pre_build_image: true

provisioner:
  name: ansible
  config_options:
    defaults:
      interpreter_python: auto_silent
      callback_whitelist: profile_tasks, timer, yaml
    ssh_connection:
      pipelining: false
```

10. Create instance

```
molecule create
```

11. Add first task - Install httpd
12. Converge the instance

```
molecule converge
```

13. Add second task - Start service
14. Converge the instance

```
molecule converge
```

15. Edit `molecule/default/molecule.yml` to enable systemd image

```
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
    privileged: true
    command: "/usr/sbin/init"
```

16. Destroy instance

```
molecule destroy
```

17. Re-create instance

```
molecule create
```

18. Converge

```
molecule converge
```

19. Add new platform to `molecule/default/molecule.yml`

```
  - name: ubuntu
    image: geerlingguy/docker-ubuntu2004-ansible
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
    privileged: true
    command: "/lib/systemd/systemd"
    pre_build_image: true
```

20. Destroy instance

```
molecule destroy
```

21. Create both instances

```
molecule create
```

22. Converge both instances

```
molecule converge
```

23. Add variables

```
cat vars/Debian.yaml

---
httpd_package: apache2
httpd_service: apache2

cat vars/RedHat.yaml
---
httpd_package: httpd
httpd_service: httpd
```

```
cat tasks/main.yml
---
# tasks file for role2
- name: Include OS-specific variables.
  include_vars: "{{ ansible_os_family }}.yaml"

- name: Ensure package cache up-to-date
  apt:
    update_cache: yes
    cache_valid_time: 3600
  when: ansible_os_family == "Debian"

- name: Ensure httpd package installed
  package:
    name: "{{ httpd_package }}"
    state: present

- name: Ensure httpd service is started
  service:
    name: "{{ httpd_service }}"
    state: started
    enabled: true
```

24. Converge instances again

```
molecule converge
```

25. Add vars for httpd user

```
Debian:
  httpd_group: www-data

CentOS:
  httpd_group: apache
```

26. Add HTML to `tasks/main.yml`

```
- name: Ensure HTML Index
  copy:
    dest: /var/www/html/index.html
    mode: 0644
    owner: root
    group: "{{ httpd_group }}"
    content: "{{ web_content }}"
```

27. Define default value for web_content var

```
cat defaults/main.yml
---
# defaults file for mywebapp
web_content: There's a web server here
```

28. Converge

```
molecule converge
```

29. Login into one of the instances to check

```
molecule login -h centos
```

30. Add verify

```
---
# This is an example playbook to execute Ansible tests.

- name: Verify
  hosts: all
  vars:
    expected_content: "There's a web server here"
  tasks:
  - name: Get index.html
    uri:
      url: http://localhost
      return_content: yes 
    register: this
    failed_when: "expected_content not in this.content"

  - name: Ensure content type is text/html
    assert:
      that:
      - "'text/html' in this.content_type"

  - name: Debug results
    debug:
      var: this.content
```

31. Verify role works

```
molecule verify
```

32. Update converge and verify playbook with new content variable

```
        web_content: "New content for testing only"
```

33. Verify again

```
molecule verify
```

34. Execute a complete test workflow

```
molecule test
```

35. Show CI/CD example
36. Show removing step (idempotence) example

```
scenario:
  test_sequence:
    - dependency
    - lint
    - cleanup
    - destroy
    - syntax
    - create
    - prepare
    - converge
    # - idempotence
    - side_effect
    - verify
    - cleanup
    - destroy
```
